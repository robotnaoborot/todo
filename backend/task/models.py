from django.db import models
from django.contrib.auth.models import User


class Task(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField('Title', max_length=255)
    description = models.TextField('Description', null=True, blank=True)
    datetime = models.DateTimeField('Date', db_index=True)

