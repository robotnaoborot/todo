from rest_framework import viewsets, filters
from rest_framework.permissions import BasePermission, IsAuthenticated, SAFE_METHODS
import django_filters
from django_filters import FilterSet
from .models import Task
from .serializers import TaskSerializer


class TaskFilter(FilterSet):
    start_date = django_filters.DateFilter(name="datetime", lookup_type='gte')
    end_date = django_filters.DateFilter(name="datetime", lookup_type='lte')

    class Meta:
        model = Task
        fields = ['start_date', 'end_date',]


class TaskViewSet(viewsets.ModelViewSet):
    serializer_class = TaskSerializer
    permission_classes = [IsAuthenticated]
    queryset = Task.objects.all()
    filter_class = TaskFilter


    def get_queryset(self):
        return Task.objects.filter(user=self.request.user)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


