from django.contrib import admin
from .models import Task


class TaskAdmin(admin.ModelAdmin):
    list_display = ('user', 'title', 'datetime')
    list_filter = ('user', )
    date_hierarchy = 'datetime'


admin.site.register(Task, TaskAdmin)

